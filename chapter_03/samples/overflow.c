#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

// undefined or wrong for most negative value
#define ABS(i) ((i) < 0 ? -(i) : (i))
// using a type-depent flag
#define ABSM(i, flag) ((i) < 0 ? -(i) : ((i) == (flag) ? (flag) : -(i)))

int main()
{
  //signed int si = -25;
  signed int si = INT_MIN;      // Will cause undefined behavior
  signed int abs_si = ABS(si);
  signed int abs_si_2 = ABSM(si, INT_MIN);

  if (abs_si == INT_MIN)
    goto recover;         // special case
  else
    printf("%d\n", abs_si2);


  printf("%d\n", abs_si);       // prints 25

  return 0;
}