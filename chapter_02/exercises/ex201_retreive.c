/**
 * Add a retrieve function to the counting example from Listing 2-6 to
 * retrieve the current value of counter.
*/

#include <stdio.h>
#include <stdlib.h>

void increment(void);
void retreive(int curr);

int main()
{
  for (int i = 0; i < 5; i++) {
    increment();
  }

  printf("\n");

  return 0;
}

void increment(void)
{
  static unsigned int counter = 0;
  counter++;
  retreive(counter);
  printf("%d ", counter);
}

void retreive(int curr)
{
  printf("Current value: %d\n", curr);
}