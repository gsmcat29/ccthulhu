/**
 * Declare an array of three pointers to functions and invoke the appropriate 
 * function based on an index value passed in as an argument.
*/

#include <stdio.h>
#include <stdlib.h>

int sum(int a, int b);
int sub(int a, int b);
int mul(int a, int b);

int (*ptrFunc[3])(int a, int b);

int main()
{
  int x = 10;
  int y = 5;

  int operation = 0;
  int value = 0;

  // assign each function to the array
  ptrFunc[0] = sum;
  ptrFunc[1] = sub;
  ptrFunc[2] = mul;

  puts("Type a value for operation: ");
  puts("0 sum, 1 sub, 2 mul: ");
  scanf("%d", &value);

  // to call those functions
  operation = (*ptrFunc[value])(x, y);

  printf("result: %d\n", operation);

  return 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
int sum(int a, int b)
{
  return (a+b);
}

int sub(int a, int b)
{
  return (a-b);
}

int mul(int a, int b)
{
  return (a*b);
}

// souce:
// https://stackoverflow.com/questions/252748/how-can-i-use-an-array-of-function-pointers