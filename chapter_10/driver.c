#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "isprime.h"

// Print command line help text
static void print_help(void)
{
  printf("%s", "primetest num [num2 num3 ... numN]\n\n");
  printf("%s", "Test positive integers for primality. Supports testing ");
  printf("%s [2-%llu].\n", "numbers in the range", ULLONG_MAX);
}

// Converts a string argumen arg to an unsigned long long value referenced by val
// Returns true if the argument conversion succeeds, and false if it fails
static bool convert_args(const char *arg, unsigned long long *val)
{
  char *end;

  // strtoll returns in-band error indicator; clear errno before the call
  errno = 0;
  *val = strtoul(arg, &end, 10);

  // check for failures where the call returns a sentinel value and sets errno
  if ((*val == ULLONG_MAX) && errno) return false;
  if (*val == 0 && errno) return false;
  if (end == arg) return false;

  // If we got here, we were able to convert the argument. However, we want to
  // allow only values greater that one, so we reject values <= 1
  if (*val <= 1) return false;

  return true;
}

static unsigned long long *convert_command_line_args(int argc, 
                                                    const char *argv[], 
                                                    size_t *num_args)
{
  *num_args = 0;

  if (argc <= 1) {
    // no command line argunments given (the first argument is the name of 
    // the program beign executed)
    print_help();
    return NULL;
  }

  /**
    *
    * We know the maximum number of arguments the user could have passed,
    * so allocate an array large enough to hold all of the elements. Subtract
    * one for the program name itself. If the locationfails, treat it as a failed
    * conversion (it is okay to call free(NULL))
  */
  unsigned long long *args = 
    (unsigned long long *)malloc(sizeof(unsigned long long) * (argc - 1));

  bool failed_conversion = (args == NULL);

  for (int i = 1; i < argc && !failed_conversion; i++) {
    // attempt to convert the argument into an integer. If we couldn't convert it,
    // set failed_conversion to true
    unsigned long long one_arg;
    failed_conversion |= !convert_args(argv[i], &one_arg);
    args[i - 1] = one_arg;
  }
 
  if (failed_conversion) {
    // free array, print the help, and bail out
    free(args);
    print_help();
    return NULL;
  }

  *num_args = argc - 1;

  return args;
}

bool is_prime(unsigned long long n, unsigned int k) {
  if (n <= 1 || n == 4) return false;
  if (n <= 3) return true;

  unsigned long long d = n -1;
  while (d % 2 == 0) d /= 2;
  
  for (; k != 0; --k) {
    if (!miller_rabin_test(d, n)) return false;
  }

  return true;
}

int main (int argc, char *argv[])
{
  size_t num_args;
  unsigned long long *vals = convert_command_line_args(argc, argv, &num_args);

  if (!vals) return EXIT_FAILURE;

  for (size_t i = 0; i < num_args; ++i) {
    printf("%llu is %s\n", vals[i], is_prime(vals[i], 100) ? "probably prime" : "not prime");
  }

  free(vals);

  return EXIT_SUCCESS;
}
